---
title: "Installation instructions"
subtitle: 
comments: false
weight: 4500

---

#### Step by step
1. Copy the [SDMX.mez](https://gitlab.com/sis-cc/sdmx-tools/sdmx-power-bi/-/blob/master/SDMX.mez) file to the folder in [Documents folder]\Power BI Desktop\Custom connectors (you may have to create this folder).
2. Open Power BI Desktop.
3. Go to File menu in Power BI Desktop.
4. Navigate to Options and Settings -> Options menu.
5. Select Security tab.
6. Under Data Extensions section, by default, "(Recommended) Only allow certified extensions to load" radio button will be selected. Switch it to "(Not Recommended) Allow any extension to load without validation or warning".
7. Now click on the OK button and restart Power BI to make the changes effective.
8. In the Get Data connector list, just search for "SIS-CC", or "SDMX".
