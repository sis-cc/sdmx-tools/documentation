---
title: "How to use"
subtitle: 
comments: false
weight: 4700

---

#### Step by step
1. Open Power BI Desktop.
2. Open Get Data, and type SDMX in the search box to quickly find this connector.
3. In the "Data query URL" textbox put an SDMX REST data query. 
Examples:
    - Pacific Data Hub "Pocket Summary": https://stats-nsi-stable.pacificdata.org/rest/data/SPC,DF_POCKET,3.0/A..?startPeriod=2020&endPeriod=2021&dimensionAtObservation=AllDimensions

4. In the "Display format" dropdown make a selection.
5. (optional) In the "Label language preference" specify a language using IETF BCP 47 format.
6. Click ok and the data should appear in the Preview screen with the standard Power BI transform optional step.
7. The dataset concepts will appear in the Fields pane to construct your report.
