---
title: "Using SDMX Matrix Generator"
subtitle: 
comments: false
weight: 8000

---

*Coming soon...*

The current user guide can be downloaded from [here](https://gitlab.com/sis-cc/sdmx-tools/sdmx-matrix-generator/-/blob/master/SDMX%20Matrix%20Generator%20User%20Guide.pptx).
